package com.example.joorebelo.fitnesstracking;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.ParticleEvent;
import io.particle.android.sdk.cloud.ParticleEventHandler;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txtTitle, txtJumpsLeft;
    Button btnAddJumps;
    EditText edtJumps;
    int jumpsLeft = 0;
    int jumpsDaily = 0;

    //region account
    // MARK: Particle Account Info
    private final String TAG="Joao";
    private final String PARTICLE_USERNAME = "joao.rebelo92@gmail.com";
    private final String PARTICLE_PASSWORD = "QWEparticle1";

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "340036000f47363333343437";
    //endregion

    // MARK: Particle Publish / Subscribe variables
    private long subscriptionId;

    // MARK: Particle device
    private ParticleDevice mDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtTitle = findViewById(R.id.txtTitle);
        txtJumpsLeft = findViewById(R.id.txtJumpsLeft);

        edtJumps = findViewById(R.id.edtJumps);

        btnAddJumps = findViewById(R.id.btnAddJumps);
        btnAddJumps.setOnClickListener(this);

        //SharedPreferences
        SharedPreferences sp = getSharedPreferences("com.example.joorebelo.fitnesstracking.shared", Context.MODE_PRIVATE);
        jumpsLeft = sp.getInt("jumpsLeft", 0);
        Toast.makeText(getApplicationContext(), "jumpsLeft: "+ jumpsLeft, Toast.LENGTH_LONG).show();
        if (jumpsLeft == 0){
            txtJumpsLeft.setText("Jumps Left: " + 0);
            Toast.makeText(getApplicationContext(), "Insert a number of jumps.", Toast.LENGTH_LONG).show();
        }else {
            jumpsDaily = sp.getInt("jumpsDaily", 0);
            txtJumpsLeft.setText("Jumps Left: " + jumpsLeft);
        }

        // 1. Initialize your connection to the Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // 2. Setup your device variable
        getDeviceFromCloud();


        startListener();
    }

    //Custom function to connect to the Particle Cloud and get the device
    public void getDeviceFromCloud() {
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                mDevice = particleCloud.getDevice(DEVICE_ID);
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    private void startListener() {
        if (mDevice == null) {
            Log.d(TAG, "Cannot find the device");
            return;
        }
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                subscriptionId = ParticleCloudSDK.getCloud().subscribeToAllEvents(
                        "broadcastMessage",  // the first argument, "eventNamePrefix", is optional
                        new ParticleEventHandler() {
                            public void onEvent(String eventName, ParticleEvent event) {
                                Log.i(TAG, "Received event with payload: " + event.dataPayload);
                                Log.i(TAG, "deviceId: " + event.deviceId);
                                Thread thread = new Thread() {
                                    @Override
                                    public void run() {
                                        try { Thread.sleep(1000); }
                                        catch (InterruptedException e) {}

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if(jumpsLeft > 1){
                                                    jumpsLeft--;
                                                    txtJumpsLeft.setText("Jumps Left: " + jumpsLeft);
                                                    int aux1 = ((jumpsDaily-jumpsLeft)*100)/11;
                                                    int aux2 = (aux1*11)/100;
                                                    sendMessage(String.valueOf(aux2));

                                                    SharedPreferences sp = getSharedPreferences("com.example.joorebelo.fitnesstracking.shared", Context.MODE_PRIVATE);
                                                    SharedPreferences.Editor edit = sp.edit();
                                                    edit.putInt("jumpsLeft", jumpsLeft);
                                                    edit.commit();
                                                }
                                            }
                                        });
                                    }
                                };
                                thread.start();


                            }

                            public void onEventError(Exception e) {
                                Log.e(TAG, "Event error: ", e);
                            }
                        });
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnAddJumps.getId()){
            String jump = edtJumps.getText().toString();
            try {
                jumpsLeft = Integer.parseInt(jump);
                SharedPreferences sp = getSharedPreferences("com.example.joorebelo.fitnesstracking.shared", Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = sp.edit();
                edit.putInt("jumpsLeft", Integer.parseInt(edtJumps.getText().toString()));
                edit.putInt("jumpsDaily", Integer.parseInt(edtJumps.getText().toString()));
                edit.commit();
                txtJumpsLeft.setText("Jumps Left: " + jumpsLeft);
            } catch (NumberFormatException e) {
                Log.i("",jump+" is not a number");
                Toast.makeText(getApplicationContext(), jump + " is not a number.", Toast.LENGTH_LONG).show();
            }
        }
    }



    private void sendMessage(String commandToSend){
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {

                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add(commandToSend);

                try {
                    mDevice.callFunction("cLights", functionParameters);
                } catch (ParticleDevice.FunctionDoesNotExistException e) {
                    e.printStackTrace();
                }

                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Sent colors command to device.");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }
}
