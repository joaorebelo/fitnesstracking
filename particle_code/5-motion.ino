// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

#include "InternetButton.h"
#include "math.h"

/*Did you know that the InternetButton can detect if it's moving? It's true!
Specifically it can read when it's being accelerated. Recall that gravity
is a constant acceleration and this becomes very useful- you know the orientation!*/


InternetButton b = InternetButton();
int startPos = 0;

void setup() {
    b.begin();
    startPos = b.readZ();
    Particle.function("cLights", controlLights);
}

void loop(){
    if(b.buttonOn(2)){
        Particle.publish("broadcastMessage", "jump", 60, PUBLIC);
        delay(500);
    }
    if(b.buttonOn(3)){
        startPos = b.readZ();
        b.allLedsOff();
        delay(500);
    }
    if(b.readZ() > (startPos + 30)){
        Particle.publish("broadcastMessage", "jump", 60, PUBLIC);
        delay(500);
    }
}

int controlLights(String command){
    
    int nleds = atoi(command.c_str());
    
    for(int i = 1; i <= nleds; i++) {
        b.ledOn(i, 255, 0, 0);
    }
    
    delay(500);
}
